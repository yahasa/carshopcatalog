module.exports = function(CarMark) {
  CarMark.carModelsByYear = function(id, year, cb) {
    CarMark.findById(id, {include: {carModels: 'carModifications'}}, function(err, mark) {
      if (err) {
        cb(err);
      } else {
        if(!mark) {
          cb(null, []);
        } else {
          var models = [];
          mark = mark.toJSON();
          mark.carModels.forEach(function(model) {
            if (model.carModifications) {
              for(var i=0; i<model.carModifications.length; i++) {
                if ((!model.carModifications[i].yearstart || model.carModifications[i].yearstart<=year)&&(!model.carModifications[i].yearend || year<=model.carModifications[i].yearend)) {
                  delete model.carModifications;
                  models.push(model);
                  break;
                }
              }
            }
          });
          // отсортируем ответ
          models.sort(function (a, b) {
            if (a.name > b.name) return 1;
            if (a.name < b.name) return -1;
            return 0;
          });
          cb(null, models);
        }
      }
    });
  };

  CarMark.remoteMethod('carModelsByYear', {
    accepts: [
      {arg: 'id', type: 'number', required: true},
      {arg: 'year', type: 'number', required: true}
    ],
    returns: {root: true, type: 'array'},
    http: {path: '/:id/carModelsByYear/:year', verb: 'get'}
  });
};
