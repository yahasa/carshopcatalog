module.exports = function(CarModel) {
  CarModel.carModificationsByYear = function(id, year, cb) {
    CarModel.findById(id, {include: 'carModifications'}, function(err, model) {
      if (err) {
        cb(err);
      } else {
        if(!model) {
          cb(null, []);
        } else {
          var modifications = [];
          model = model.toJSON();
          model.carModifications.forEach(function(modfn) {
            if ((!modfn.yearstart || modfn.yearstart<=year)&&(!modfn.yearend || year<=modfn.yearend)) {
              modifications.push(modfn);
            }
          });
          modifications.sort(function (a, b) {
            if (a.name > b.name) return 1;
            if (a.name < b.name) return -1;
            return 0;
          });
          // отсортируем ответ
          cb(null, modifications);
        }
      }
    });
  };

  CarModel.remoteMethod('carModificationsByYear', {
    accepts: [
      {arg: 'id', type: 'number', required: true},
      {arg: 'year', type: 'number', required: true}
    ],
    returns: {root: true, type: 'array'},
    http: {path: '/:id/carModificationsByYear/:year', verb: 'get'}
  });
};
