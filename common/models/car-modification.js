module.exports = function(CarModification) {
  CarModification.importAutoRuModifications = function(filename, cb) {
    var importModifications = require('../../scripts/db-import-autoru-cars-catalog').importModifications;
    var path = require('path');
    filename = path.join(__dirname, '../../assets/storage', filename);
    importModifications(CarModification.app, filename, function(err, message) {
      if (err) {
        cb(err, message);
      } else {
        cb(null, message);
      }
    });
    //try {
    //  message = importModifications(CarModification.app, filename);
    //} catch (err) {
    //  message = 'error';
    //  cb(err, message);
    //}
    //cb(null, message);
  };

  CarModification.remoteMethod('importAutoRuModifications', {
    accepts: {arg: 'filename', type: 'string', required: true},
    returns: {arg: 'message', type: 'string'}
  });
};
