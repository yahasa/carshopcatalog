var gulp = require('gulp');
var rename = require('gulp-rename');
var loopbackAngular = require('gulp-loopback-sdk-angular');
var inject = require('gulp-inject');

var browserSync = require('browser-sync').create();
var reload = browserSync.reload;

gulp.task('lbng', function () {
  return gulp.src('./server/server.js')
    .pipe(loopbackAngular())
    .pipe(rename('lb-services.js'))
    .pipe(gulp.dest('./client/app/services'));
});


gulp.task('watch', function () {
  browserSync.init({
    proxy: "localhost:3000"
  });

  gulp.watch(["client/*.html", "client/src/**"]).on('change', reload);
});

gulp.task('inject-js-dev', function () {
  var target = gulp.src('./client/index.html');
  var sources = gulp.src(['./client/app/**/*.module.js', './client/app/**/*.js'], {read: false});
  return target
    .pipe(inject(sources, {relative: true}))
    .pipe(gulp.dest('./client'));
});

gulp.task('default', ['watch']);
