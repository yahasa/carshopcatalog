/**
 * @file Импорт автомобилей из архива xml файла каталога auto.ru
 * @const IMPORTFILE - путь к json файлу каталога автомобилей по-умолчанию
 */
"use strict";

var AUTORUXML = '../assets/storage/import/autoru_cars_catalog.zip';
var fs = require('fs');
var JSZip = require("jszip");
var path = require('path');
var async = require('async');
var xml2js = require('xml2js');
var parser = new xml2js.Parser();

module.exports.importModifications = importModifications;

if (require.main === module) {
  var app = require('../server/server.js');
  importModifications(app, path.join(__dirname, AUTORUXML), function(err, msg) {
    if (err) {
      console.log(msg);
      console.log(err);
    } else {
      console.log(msg);
    }
  });
}

/**
 * Импорт модификаций из файла autoru_cars_catalog.xml, запакованного в архив
 * @param {Object} app Экземпляр приложения loopback
 * @param {String} filename Путь к файлу импорта
 * @param {Function} [callback]
 */
// TODO Разрешить импорт из незапакованного файла, проверять, архив или нет
// TODO Обработать ошибки
function importModifications(app, filename, callback) {
  var CarMark = app.models.CarMark;
  var CarModel = app.models.CarModel;
  var CarModification = app.models.CarModification;
  fs.readFile(filename, function (err, data) {
    if (err) {
      console.log('Не удалось прочитать файл: ' + filename);
      //throw err;
      callback(err, 'Не удалось прочитать файл: ' + filename);
    } else {
      var zip = new JSZip();
      try {
        zip.load(data, {checkCRC32: true});
      } catch(err) {
        console.log('Файл должен быть архивом');
        callback(new Error('File must be an archive'), 'Файл должен быть архивом');
        return;
      }
      var xmlZip = zip.file('autoru_cars_catalog.xml');
      if (!xmlZip) {
        console.log('Внутри архива должен быть файл autoru_cars_catalog.xml');
        //throw new Error('File \"autoru_cars_catalog.xml\" must exist in archive');
        callback(new Error('File \"autoru_cars_catalog.xml\" must exist in archive'), 'Внутри архива должен быть файл autoru_cars_catalog.xml');
      } else {
        parser.parseString(xmlZip.asText(), function (err, result) {
          if (err) {
            console.log('Не удалось распарсить файл: ' + filename);
            //throw err;
            callback(err, 'Не удалось распарсить файл: ' + filename);
          } else {
            console.log('Начало выполнения скрипта импорта модификаций: ', new Date());
            console.time('Время импорта модификаций');
            async.mapSeries(result.catalog.mark, function (mark, cb) {

              var markName = mark.$.name;
              CarMark.findOrCreate({where: {name: markName}}, {name: markName}, function(err, markInstance) {
                if (err) {
                  console.log('Не удалось создать марку: ' + markName);
                  cb(err);
                } else {
                  // Для каждой марки импортируем модели
                  async.mapSeries(mark.folder, function(model, cb) {

                    // Разделим строки вида "Camry, III (XV10)"
                    // !!! Исходим из того, что в модели одна запятая
                    var splits = model.$.name.split(', ');
                    var modelName = splits[0];
                    var generationName = splits[1] || null;
                    CarModel.findOrCreate({where: {name: modelName, carMarkId: markInstance.id}}, {name: modelName, carMarkId: markInstance.id}, function(err, modelInstance) {
                      if (err) {
                        console.log('Не удалось создать модель: [' + markName + '] ' + modelName);
                        cb(err);
                      } else {
                        // Для каждой модели импортируем модификации
                        async.mapSeries(model.modification, function(modification, cb) {

                          var modificationName = modification.$.name;
                          var bodyType = (modification.body_type && modification.body_type[0]) || null;
                          var years = parseYears(modification.years ? modification.years[0] : null);

                          CarModification.findOrCreate({where: {
                            name: modificationName,
                            carModelId: modelInstance.id,
                            generation: generationName,
                            bodytype: bodyType,
                            yearstart: years.yearstart,
                            yearend: years.yearend
                          }}, {
                            name: modificationName,
                            carModelId: modelInstance.id,
                            generation: generationName,
                            bodytype: bodyType,
                            yearstart: years.yearstart,
                            yearend: years.yearend
                          }, function(err, modificationInstance) {
                            if (err) {
                              console.log('Не удалось создать модификацию: [' + markName + '] [' + modelName + '] ' + modificationName);
                              cb(err);
                            } else {
                              //console.log('Обработана модификация: ' + JSON.stringify(modification));
                              cb(null, modificationName);
                            }
                          });
                        }, function(err, modifications) {
                          if (err) throw err;
                          //console.log('Для модели ' + modelName + ' обработано ' + modifications.length + ' модификаций');
                          cb(null, modelName);
                        });
                      }
                    });
                  }, function(err, models) {
                    if (err) throw err;
                    console.log('Для марки ' + markName + ' обработано ' + models.length + ' моделей');
                    cb(null, markName);
                  })
                }
              });
            }, function(err, marks) {
              if (err) throw err;
              console.log('Импорт завершен. Обработано ' + marks.length + ' марок автомобилей');
              console.timeEnd('Время импорта модификаций');
              callback(null, 'Импорт завершен. Обработано ' + marks.length + ' марок автомобилей');
            });
          }
        });
      }
    }
  });
}

/**
 * Переводит строки вида "2005-2010" в объект годов
 * TODO Константы MINYEAR, MAXYEAR (диапазон допустимых годов) можно вынести куда-нибудь в настройки
 * @param yearsstring [String] Строка вида "2005-2010", "2010 - по н.в."
 * @returns {{yearstart: Number | null, yearend: Number | null}}
 */
function parseYears(yearsstring) {
  // максимальное и минимальное допустимые значения годов
  var MAXYEAR = new Date().getFullYear();
  var MINYEAR = 1950;
  var years = {
    yearstart: null,
    yearend: null
  };
  if(!yearsstring) return years;

  // регулярное выражение для парсинга годов
  var yearsRegExp = /^(\d{4}|^)\s*-\s*(\d{4}|по\sн\.в\.)/;
  var yearsRegExpResult = yearsRegExp.exec(yearsstring);
  if (yearsRegExpResult) {
    years.yearstart = parseInt(yearsRegExpResult[1]);
    years.yearend = parseInt(yearsRegExpResult[2]);
    // проверка на попадание года в диапазон
    if (!years.yearstart || years.yearstart < MINYEAR) {
      years.yearstart = null;
    }
    if (!years.yearend || years.yearend > MAXYEAR) {
      years.yearend = null;
    }
  } else {
    // неправильная строка годов в файле импорта
    console.log("Ошибка парсинга строки годов: " + yearsstring);
  }
  return years;
}
