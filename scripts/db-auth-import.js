/*
 * @file Создание тестовых пользователей и ролей
 * @const IMPORTFILE - путь к json файлу пользователей
 * */
"use strict";

// TODO Добавить роли

var IMPORTFILE = '../assets/auth.json';
//var async = require('async');
var app = require('../server/server.js');
var User = app.models.User;
//var Role = app.models.Role;
//var RoleMapping = app.models.RoleMapping;

var auth = require(IMPORTFILE);

//console.time('Скрипт завершен.');
auth.users.forEach(function(user) {
  User.findOrCreate({where: {username: user.username, email: user.email}}, user, function (err, userInstance) {
    console.log('Пользователь \"' + userInstance.username + '\" добавлен.');
  });
});
