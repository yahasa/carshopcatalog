/**
 * @file Обновление моделей в базе с УДАЛЕНИЕМ данных
 */
var app = require('../server/server.js');
var db = app.datasources.db;

db.automigrate(function() {
  console.log('db upgraded');
});
