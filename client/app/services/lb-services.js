(function(window, angular, undefined) {'use strict';

var urlBase = "/api";
var authHeader = 'authorization';

/**
 * @ngdoc overview
 * @name lbServices
 * @module
 * @description
 *
 * The `lbServices` module provides services for interacting with
 * the models exposed by the LoopBack server via the REST API.
 *
 */
var module = angular.module("lbServices", ['ngResource']);

/**
 * @ngdoc object
 * @name lbServices.User
 * @header lbServices.User
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `User` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "User",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/Users/:id",
      { 'id': '@id' },
      {

        /**
         * @ngdoc method
         * @name lbServices.User#prototype$__findById__accessTokens
         * @methodOf lbServices.User
         *
         * @description
         *
         * Find a related item by id for accessTokens.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - User id
         *
         *  - `fk` – `{*}` - Foreign key for accessTokens
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `User` object.)
         * </em>
         */
        "prototype$__findById__accessTokens": {
          url: urlBase + "/Users/:id/accessTokens/:fk",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.User#prototype$__destroyById__accessTokens
         * @methodOf lbServices.User
         *
         * @description
         *
         * Delete a related item by id for accessTokens.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - User id
         *
         *  - `fk` – `{*}` - Foreign key for accessTokens
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "prototype$__destroyById__accessTokens": {
          url: urlBase + "/Users/:id/accessTokens/:fk",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name lbServices.User#prototype$__updateById__accessTokens
         * @methodOf lbServices.User
         *
         * @description
         *
         * Update a related item by id for accessTokens.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - User id
         *
         *  - `fk` – `{*}` - Foreign key for accessTokens
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `User` object.)
         * </em>
         */
        "prototype$__updateById__accessTokens": {
          url: urlBase + "/Users/:id/accessTokens/:fk",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name lbServices.User#prototype$__get__accessTokens
         * @methodOf lbServices.User
         *
         * @description
         *
         * Queries accessTokens of User.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - User id
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `User` object.)
         * </em>
         */
        "prototype$__get__accessTokens": {
          isArray: true,
          url: urlBase + "/Users/:id/accessTokens",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.User#prototype$__create__accessTokens
         * @methodOf lbServices.User
         *
         * @description
         *
         * Creates a new instance in accessTokens of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - User id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `User` object.)
         * </em>
         */
        "prototype$__create__accessTokens": {
          url: urlBase + "/Users/:id/accessTokens",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.User#prototype$__delete__accessTokens
         * @methodOf lbServices.User
         *
         * @description
         *
         * Deletes all accessTokens of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - User id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "prototype$__delete__accessTokens": {
          url: urlBase + "/Users/:id/accessTokens",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name lbServices.User#prototype$__count__accessTokens
         * @methodOf lbServices.User
         *
         * @description
         *
         * Counts accessTokens of User.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - User id
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "prototype$__count__accessTokens": {
          url: urlBase + "/Users/:id/accessTokens/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.User#create
         * @methodOf lbServices.User
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `User` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/Users",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.User#upsert
         * @methodOf lbServices.User
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `User` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/Users",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name lbServices.User#exists
         * @methodOf lbServices.User
         *
         * @description
         *
         * Check whether a model instance exists in the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/Users/:id/exists",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.User#findById
         * @methodOf lbServices.User
         *
         * @description
         *
         * Find a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         *  - `filter` – `{object=}` - Filter defining fields and include
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `User` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/Users/:id",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.User#find
         * @methodOf lbServices.User
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `User` object.)
         * </em>
         */
        "find": {
          isArray: true,
          url: urlBase + "/Users",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.User#findOne
         * @methodOf lbServices.User
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `User` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/Users/findOne",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.User#updateAll
         * @methodOf lbServices.User
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/Users/update",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.User#deleteById
         * @methodOf lbServices.User
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/Users/:id",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name lbServices.User#count
         * @methodOf lbServices.User
         *
         * @description
         *
         * Count instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/Users/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.User#prototype$updateAttributes
         * @methodOf lbServices.User
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - User id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `User` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/Users/:id",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name lbServices.User#login
         * @methodOf lbServices.User
         *
         * @description
         *
         * Login a user with username/email and password.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `include` – `{string=}` - Related objects to include in the response. See the description of return value for more details.
         *   Default value: `user`.
         *
         *  - `rememberMe` - `boolean` - Whether the authentication credentials
         *     should be remembered in localStorage across app/browser restarts.
         *     Default: `true`.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * The response body contains properties of the AccessToken created on login.
         * Depending on the value of `include` parameter, the body may contain additional properties:
         * 
         *   - `user` - `{User}` - Data of the currently logged in user. (`include=user`)
         * 
         *
         */
        "login": {
          params: {
            include: "user"
          },
          interceptor: {
            response: function(response) {
              var accessToken = response.data;
              LoopBackAuth.setUser(accessToken.id, accessToken.userId, accessToken.user);
              LoopBackAuth.rememberMe = response.config.params.rememberMe !== false;
              LoopBackAuth.save();
              return response.resource;
            }
          },
          url: urlBase + "/Users/login",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.User#logout
         * @methodOf lbServices.User
         *
         * @description
         *
         * Logout a user with access token
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         *  - `access_token` – `{string}` - Do not supply this argument, it is automatically extracted from request headers.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "logout": {
          interceptor: {
            response: function(response) {
              LoopBackAuth.clearUser();
              LoopBackAuth.clearStorage();
              return response.resource;
            }
          },
          url: urlBase + "/Users/logout",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.User#confirm
         * @methodOf lbServices.User
         *
         * @description
         *
         * Confirm a user registration with email verification token
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `uid` – `{string}` - 
         *
         *  - `token` – `{string}` - 
         *
         *  - `redirect` – `{string=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "confirm": {
          url: urlBase + "/Users/confirm",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.User#resetPassword
         * @methodOf lbServices.User
         *
         * @description
         *
         * Reset password for a user with email
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "resetPassword": {
          url: urlBase + "/Users/reset",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.User#getCurrent
         * @methodOf lbServices.User
         *
         * @description
         *
         * Get data of the currently logged user. Fail with HTTP result 401
         * when there is no user logged in.
         *
         * @param {function(Object,Object)=} successCb
         *    Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *    `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         */
        "getCurrent": {
           url: urlBase + "/Users" + "/:id",
           method: "GET",
           params: {
             id: function() {
              var id = LoopBackAuth.currentUserId;
              if (id == null) id = '__anonymous__';
              return id;
            },
          },
          interceptor: {
            response: function(response) {
              LoopBackAuth.currentUserData = response.data;
              return response.resource;
            }
          },
          __isGetCurrentUser__ : true
        }
      }
    );



        /**
         * @ngdoc method
         * @name lbServices.User#updateOrCreate
         * @methodOf lbServices.User
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `User` object.)
         * </em>
         */
        R["updateOrCreate"] = R["upsert"];

        /**
         * @ngdoc method
         * @name lbServices.User#update
         * @methodOf lbServices.User
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["update"] = R["updateAll"];

        /**
         * @ngdoc method
         * @name lbServices.User#destroyById
         * @methodOf lbServices.User
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["destroyById"] = R["deleteById"];

        /**
         * @ngdoc method
         * @name lbServices.User#removeById
         * @methodOf lbServices.User
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["removeById"] = R["deleteById"];

        /**
         * @ngdoc method
         * @name lbServices.User#getCachedCurrent
         * @methodOf lbServices.User
         *
         * @description
         *
         * Get data of the currently logged user that was returned by the last
         * call to {@link lbServices.User#login} or
         * {@link lbServices.User#getCurrent}. Return null when there
         * is no user logged in or the data of the current user were not fetched
         * yet.
         *
         * @returns {Object} A User instance.
         */
        R.getCachedCurrent = function() {
          var data = LoopBackAuth.currentUserData;
          return data ? new R(data) : null;
        };

        /**
         * @ngdoc method
         * @name lbServices.User#isAuthenticated
         * @methodOf lbServices.User
         *
         * @returns {boolean} True if the current user is authenticated (logged in).
         */
        R.isAuthenticated = function() {
          return this.getCurrentId() != null;
        };

        /**
         * @ngdoc method
         * @name lbServices.User#getCurrentId
         * @methodOf lbServices.User
         *
         * @returns {Object} Id of the currently logged-in user or null.
         */
        R.getCurrentId = function() {
          return LoopBackAuth.currentUserId;
        };

    /**
    * @ngdoc property
    * @name lbServices.User#modelName
    * @propertyOf lbServices.User
    * @description
    * The name of the model represented by this $resource,
    * i.e. `User`.
    */
    R.modelName = "User";


    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.CarMark
 * @header lbServices.CarMark
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `CarMark` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "CarMark",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/CarMarks/:id",
      { 'id': '@id' },
      {

        // INTERNAL. Use CarMark.carModels.findById() instead.
        "prototype$__findById__carModels": {
          url: urlBase + "/CarMarks/:id/carModels/:fk",
          method: "GET"
        },

        // INTERNAL. Use CarMark.carModels.destroyById() instead.
        "prototype$__destroyById__carModels": {
          url: urlBase + "/CarMarks/:id/carModels/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use CarMark.carModels.updateById() instead.
        "prototype$__updateById__carModels": {
          url: urlBase + "/CarMarks/:id/carModels/:fk",
          method: "PUT"
        },

        // INTERNAL. Use CarMark.carModels() instead.
        "prototype$__get__carModels": {
          isArray: true,
          url: urlBase + "/CarMarks/:id/carModels",
          method: "GET"
        },

        // INTERNAL. Use CarMark.carModels.create() instead.
        "prototype$__create__carModels": {
          url: urlBase + "/CarMarks/:id/carModels",
          method: "POST"
        },

        // INTERNAL. Use CarMark.carModels.destroyAll() instead.
        "prototype$__delete__carModels": {
          url: urlBase + "/CarMarks/:id/carModels",
          method: "DELETE"
        },

        // INTERNAL. Use CarMark.carModels.count() instead.
        "prototype$__count__carModels": {
          url: urlBase + "/CarMarks/:id/carModels/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.CarMark#create
         * @methodOf lbServices.CarMark
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `CarMark` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/CarMarks",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.CarMark#upsert
         * @methodOf lbServices.CarMark
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `CarMark` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/CarMarks",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name lbServices.CarMark#exists
         * @methodOf lbServices.CarMark
         *
         * @description
         *
         * Check whether a model instance exists in the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/CarMarks/:id/exists",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.CarMark#findById
         * @methodOf lbServices.CarMark
         *
         * @description
         *
         * Find a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         *  - `filter` – `{object=}` - Filter defining fields and include
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `CarMark` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/CarMarks/:id",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.CarMark#find
         * @methodOf lbServices.CarMark
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `CarMark` object.)
         * </em>
         */
        "find": {
          isArray: true,
          url: urlBase + "/CarMarks",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.CarMark#findOne
         * @methodOf lbServices.CarMark
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `CarMark` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/CarMarks/findOne",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.CarMark#updateAll
         * @methodOf lbServices.CarMark
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/CarMarks/update",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.CarMark#deleteById
         * @methodOf lbServices.CarMark
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/CarMarks/:id",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name lbServices.CarMark#count
         * @methodOf lbServices.CarMark
         *
         * @description
         *
         * Count instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/CarMarks/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.CarMark#prototype$updateAttributes
         * @methodOf lbServices.CarMark
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `CarMark` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/CarMarks/:id",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name lbServices.CarMark#carModelsByYear
         * @methodOf lbServices.CarMark
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{number}` - 
         *
         *  - `year` – `{number}` - 
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `CarMark` object.)
         * </em>
         */
        "carModelsByYear": {
          isArray: true,
          url: urlBase + "/CarMarks/:id/carModelsByYear/:year",
          method: "GET"
        },

        // INTERNAL. Use CarModel.carMark() instead.
        "::get::CarModel::carMark": {
          url: urlBase + "/CarModels/:id/carMark",
          method: "GET"
        },
      }
    );



        /**
         * @ngdoc method
         * @name lbServices.CarMark#updateOrCreate
         * @methodOf lbServices.CarMark
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `CarMark` object.)
         * </em>
         */
        R["updateOrCreate"] = R["upsert"];

        /**
         * @ngdoc method
         * @name lbServices.CarMark#update
         * @methodOf lbServices.CarMark
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["update"] = R["updateAll"];

        /**
         * @ngdoc method
         * @name lbServices.CarMark#destroyById
         * @methodOf lbServices.CarMark
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["destroyById"] = R["deleteById"];

        /**
         * @ngdoc method
         * @name lbServices.CarMark#removeById
         * @methodOf lbServices.CarMark
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["removeById"] = R["deleteById"];


    /**
    * @ngdoc property
    * @name lbServices.CarMark#modelName
    * @propertyOf lbServices.CarMark
    * @description
    * The name of the model represented by this $resource,
    * i.e. `CarMark`.
    */
    R.modelName = "CarMark";

    /**
     * @ngdoc object
     * @name lbServices.CarMark.carModels
     * @header lbServices.CarMark.carModels
     * @object
     * @description
     *
     * The object `CarMark.carModels` groups methods
     * manipulating `CarModel` instances related to `CarMark`.
     *
     * Call {@link lbServices.CarMark#carModels CarMark.carModels()}
     * to query all related instances.
     */


        /**
         * @ngdoc method
         * @name lbServices.CarMark#carModels
         * @methodOf lbServices.CarMark
         *
         * @description
         *
         * Queries carModels of CarMark.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `CarModel` object.)
         * </em>
         */
        R.carModels = function() {
          var TargetResource = $injector.get("CarModel");
          var action = TargetResource["::get::CarMark::carModels"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.CarMark.carModels#count
         * @methodOf lbServices.CarMark.carModels
         *
         * @description
         *
         * Counts carModels of CarMark.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        R.carModels.count = function() {
          var TargetResource = $injector.get("CarModel");
          var action = TargetResource["::count::CarMark::carModels"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.CarMark.carModels#create
         * @methodOf lbServices.CarMark.carModels
         *
         * @description
         *
         * Creates a new instance in carModels of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `CarModel` object.)
         * </em>
         */
        R.carModels.create = function() {
          var TargetResource = $injector.get("CarModel");
          var action = TargetResource["::create::CarMark::carModels"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.CarMark.carModels#destroyAll
         * @methodOf lbServices.CarMark.carModels
         *
         * @description
         *
         * Deletes all carModels of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.carModels.destroyAll = function() {
          var TargetResource = $injector.get("CarModel");
          var action = TargetResource["::delete::CarMark::carModels"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.CarMark.carModels#destroyById
         * @methodOf lbServices.CarMark.carModels
         *
         * @description
         *
         * Delete a related item by id for carModels.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for carModels
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.carModels.destroyById = function() {
          var TargetResource = $injector.get("CarModel");
          var action = TargetResource["::destroyById::CarMark::carModels"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.CarMark.carModels#findById
         * @methodOf lbServices.CarMark.carModels
         *
         * @description
         *
         * Find a related item by id for carModels.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for carModels
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `CarModel` object.)
         * </em>
         */
        R.carModels.findById = function() {
          var TargetResource = $injector.get("CarModel");
          var action = TargetResource["::findById::CarMark::carModels"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.CarMark.carModels#updateById
         * @methodOf lbServices.CarMark.carModels
         *
         * @description
         *
         * Update a related item by id for carModels.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for carModels
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `CarModel` object.)
         * </em>
         */
        R.carModels.updateById = function() {
          var TargetResource = $injector.get("CarModel");
          var action = TargetResource["::updateById::CarMark::carModels"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.CarModel
 * @header lbServices.CarModel
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `CarModel` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "CarModel",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/CarModels/:id",
      { 'id': '@id' },
      {

        // INTERNAL. Use CarModel.carMark() instead.
        "prototype$__get__carMark": {
          url: urlBase + "/CarModels/:id/carMark",
          method: "GET"
        },

        // INTERNAL. Use CarModel.carGenerations.findById() instead.
        "prototype$__findById__carGenerations": {
          url: urlBase + "/CarModels/:id/carGenerations/:fk",
          method: "GET"
        },

        // INTERNAL. Use CarModel.carGenerations.destroyById() instead.
        "prototype$__destroyById__carGenerations": {
          url: urlBase + "/CarModels/:id/carGenerations/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use CarModel.carGenerations.updateById() instead.
        "prototype$__updateById__carGenerations": {
          url: urlBase + "/CarModels/:id/carGenerations/:fk",
          method: "PUT"
        },

        // INTERNAL. Use CarModel.carModifications.findById() instead.
        "prototype$__findById__carModifications": {
          url: urlBase + "/CarModels/:id/carModifications/:fk",
          method: "GET"
        },

        // INTERNAL. Use CarModel.carModifications.destroyById() instead.
        "prototype$__destroyById__carModifications": {
          url: urlBase + "/CarModels/:id/carModifications/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use CarModel.carModifications.updateById() instead.
        "prototype$__updateById__carModifications": {
          url: urlBase + "/CarModels/:id/carModifications/:fk",
          method: "PUT"
        },

        // INTERNAL. Use CarModel.carGenerations() instead.
        "prototype$__get__carGenerations": {
          isArray: true,
          url: urlBase + "/CarModels/:id/carGenerations",
          method: "GET"
        },

        // INTERNAL. Use CarModel.carGenerations.create() instead.
        "prototype$__create__carGenerations": {
          url: urlBase + "/CarModels/:id/carGenerations",
          method: "POST"
        },

        // INTERNAL. Use CarModel.carGenerations.destroyAll() instead.
        "prototype$__delete__carGenerations": {
          url: urlBase + "/CarModels/:id/carGenerations",
          method: "DELETE"
        },

        // INTERNAL. Use CarModel.carGenerations.count() instead.
        "prototype$__count__carGenerations": {
          url: urlBase + "/CarModels/:id/carGenerations/count",
          method: "GET"
        },

        // INTERNAL. Use CarModel.carModifications() instead.
        "prototype$__get__carModifications": {
          isArray: true,
          url: urlBase + "/CarModels/:id/carModifications",
          method: "GET"
        },

        // INTERNAL. Use CarModel.carModifications.create() instead.
        "prototype$__create__carModifications": {
          url: urlBase + "/CarModels/:id/carModifications",
          method: "POST"
        },

        // INTERNAL. Use CarModel.carModifications.destroyAll() instead.
        "prototype$__delete__carModifications": {
          url: urlBase + "/CarModels/:id/carModifications",
          method: "DELETE"
        },

        // INTERNAL. Use CarModel.carModifications.count() instead.
        "prototype$__count__carModifications": {
          url: urlBase + "/CarModels/:id/carModifications/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.CarModel#create
         * @methodOf lbServices.CarModel
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `CarModel` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/CarModels",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.CarModel#upsert
         * @methodOf lbServices.CarModel
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `CarModel` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/CarModels",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name lbServices.CarModel#exists
         * @methodOf lbServices.CarModel
         *
         * @description
         *
         * Check whether a model instance exists in the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/CarModels/:id/exists",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.CarModel#findById
         * @methodOf lbServices.CarModel
         *
         * @description
         *
         * Find a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         *  - `filter` – `{object=}` - Filter defining fields and include
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `CarModel` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/CarModels/:id",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.CarModel#find
         * @methodOf lbServices.CarModel
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `CarModel` object.)
         * </em>
         */
        "find": {
          isArray: true,
          url: urlBase + "/CarModels",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.CarModel#findOne
         * @methodOf lbServices.CarModel
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `CarModel` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/CarModels/findOne",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.CarModel#updateAll
         * @methodOf lbServices.CarModel
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/CarModels/update",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.CarModel#deleteById
         * @methodOf lbServices.CarModel
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/CarModels/:id",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name lbServices.CarModel#count
         * @methodOf lbServices.CarModel
         *
         * @description
         *
         * Count instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/CarModels/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.CarModel#prototype$updateAttributes
         * @methodOf lbServices.CarModel
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `CarModel` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/CarModels/:id",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name lbServices.CarModel#carModificationsByYear
         * @methodOf lbServices.CarModel
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{number}` - 
         *
         *  - `year` – `{number}` - 
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `CarModel` object.)
         * </em>
         */
        "carModificationsByYear": {
          isArray: true,
          url: urlBase + "/CarModels/:id/carModificationsByYear/:year",
          method: "GET"
        },

        // INTERNAL. Use CarMark.carModels.findById() instead.
        "::findById::CarMark::carModels": {
          url: urlBase + "/CarMarks/:id/carModels/:fk",
          method: "GET"
        },

        // INTERNAL. Use CarMark.carModels.destroyById() instead.
        "::destroyById::CarMark::carModels": {
          url: urlBase + "/CarMarks/:id/carModels/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use CarMark.carModels.updateById() instead.
        "::updateById::CarMark::carModels": {
          url: urlBase + "/CarMarks/:id/carModels/:fk",
          method: "PUT"
        },

        // INTERNAL. Use CarMark.carModels() instead.
        "::get::CarMark::carModels": {
          isArray: true,
          url: urlBase + "/CarMarks/:id/carModels",
          method: "GET"
        },

        // INTERNAL. Use CarMark.carModels.create() instead.
        "::create::CarMark::carModels": {
          url: urlBase + "/CarMarks/:id/carModels",
          method: "POST"
        },

        // INTERNAL. Use CarMark.carModels.destroyAll() instead.
        "::delete::CarMark::carModels": {
          url: urlBase + "/CarMarks/:id/carModels",
          method: "DELETE"
        },

        // INTERNAL. Use CarMark.carModels.count() instead.
        "::count::CarMark::carModels": {
          url: urlBase + "/CarMarks/:id/carModels/count",
          method: "GET"
        },

        // INTERNAL. Use CarGeneration.carModel() instead.
        "::get::CarGeneration::carModel": {
          url: urlBase + "/CarGenerations/:id/carModel",
          method: "GET"
        },

        // INTERNAL. Use CarModification.carModel() instead.
        "::get::CarModification::carModel": {
          url: urlBase + "/CarModifications/:id/carModel",
          method: "GET"
        },
      }
    );



        /**
         * @ngdoc method
         * @name lbServices.CarModel#updateOrCreate
         * @methodOf lbServices.CarModel
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `CarModel` object.)
         * </em>
         */
        R["updateOrCreate"] = R["upsert"];

        /**
         * @ngdoc method
         * @name lbServices.CarModel#update
         * @methodOf lbServices.CarModel
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["update"] = R["updateAll"];

        /**
         * @ngdoc method
         * @name lbServices.CarModel#destroyById
         * @methodOf lbServices.CarModel
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["destroyById"] = R["deleteById"];

        /**
         * @ngdoc method
         * @name lbServices.CarModel#removeById
         * @methodOf lbServices.CarModel
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["removeById"] = R["deleteById"];


    /**
    * @ngdoc property
    * @name lbServices.CarModel#modelName
    * @propertyOf lbServices.CarModel
    * @description
    * The name of the model represented by this $resource,
    * i.e. `CarModel`.
    */
    R.modelName = "CarModel";


        /**
         * @ngdoc method
         * @name lbServices.CarModel#carMark
         * @methodOf lbServices.CarModel
         *
         * @description
         *
         * Fetches belongsTo relation carMark.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `CarMark` object.)
         * </em>
         */
        R.carMark = function() {
          var TargetResource = $injector.get("CarMark");
          var action = TargetResource["::get::CarModel::carMark"];
          return action.apply(R, arguments);
        };
    /**
     * @ngdoc object
     * @name lbServices.CarModel.carGenerations
     * @header lbServices.CarModel.carGenerations
     * @object
     * @description
     *
     * The object `CarModel.carGenerations` groups methods
     * manipulating `CarGeneration` instances related to `CarModel`.
     *
     * Call {@link lbServices.CarModel#carGenerations CarModel.carGenerations()}
     * to query all related instances.
     */


        /**
         * @ngdoc method
         * @name lbServices.CarModel#carGenerations
         * @methodOf lbServices.CarModel
         *
         * @description
         *
         * Queries carGenerations of CarModel.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `CarGeneration` object.)
         * </em>
         */
        R.carGenerations = function() {
          var TargetResource = $injector.get("CarGeneration");
          var action = TargetResource["::get::CarModel::carGenerations"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.CarModel.carGenerations#count
         * @methodOf lbServices.CarModel.carGenerations
         *
         * @description
         *
         * Counts carGenerations of CarModel.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        R.carGenerations.count = function() {
          var TargetResource = $injector.get("CarGeneration");
          var action = TargetResource["::count::CarModel::carGenerations"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.CarModel.carGenerations#create
         * @methodOf lbServices.CarModel.carGenerations
         *
         * @description
         *
         * Creates a new instance in carGenerations of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `CarGeneration` object.)
         * </em>
         */
        R.carGenerations.create = function() {
          var TargetResource = $injector.get("CarGeneration");
          var action = TargetResource["::create::CarModel::carGenerations"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.CarModel.carGenerations#destroyAll
         * @methodOf lbServices.CarModel.carGenerations
         *
         * @description
         *
         * Deletes all carGenerations of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.carGenerations.destroyAll = function() {
          var TargetResource = $injector.get("CarGeneration");
          var action = TargetResource["::delete::CarModel::carGenerations"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.CarModel.carGenerations#destroyById
         * @methodOf lbServices.CarModel.carGenerations
         *
         * @description
         *
         * Delete a related item by id for carGenerations.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for carGenerations
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.carGenerations.destroyById = function() {
          var TargetResource = $injector.get("CarGeneration");
          var action = TargetResource["::destroyById::CarModel::carGenerations"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.CarModel.carGenerations#findById
         * @methodOf lbServices.CarModel.carGenerations
         *
         * @description
         *
         * Find a related item by id for carGenerations.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for carGenerations
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `CarGeneration` object.)
         * </em>
         */
        R.carGenerations.findById = function() {
          var TargetResource = $injector.get("CarGeneration");
          var action = TargetResource["::findById::CarModel::carGenerations"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.CarModel.carGenerations#updateById
         * @methodOf lbServices.CarModel.carGenerations
         *
         * @description
         *
         * Update a related item by id for carGenerations.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for carGenerations
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `CarGeneration` object.)
         * </em>
         */
        R.carGenerations.updateById = function() {
          var TargetResource = $injector.get("CarGeneration");
          var action = TargetResource["::updateById::CarModel::carGenerations"];
          return action.apply(R, arguments);
        };
    /**
     * @ngdoc object
     * @name lbServices.CarModel.carModifications
     * @header lbServices.CarModel.carModifications
     * @object
     * @description
     *
     * The object `CarModel.carModifications` groups methods
     * manipulating `CarModification` instances related to `CarModel`.
     *
     * Call {@link lbServices.CarModel#carModifications CarModel.carModifications()}
     * to query all related instances.
     */


        /**
         * @ngdoc method
         * @name lbServices.CarModel#carModifications
         * @methodOf lbServices.CarModel
         *
         * @description
         *
         * Queries carModifications of CarModel.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `CarModification` object.)
         * </em>
         */
        R.carModifications = function() {
          var TargetResource = $injector.get("CarModification");
          var action = TargetResource["::get::CarModel::carModifications"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.CarModel.carModifications#count
         * @methodOf lbServices.CarModel.carModifications
         *
         * @description
         *
         * Counts carModifications of CarModel.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        R.carModifications.count = function() {
          var TargetResource = $injector.get("CarModification");
          var action = TargetResource["::count::CarModel::carModifications"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.CarModel.carModifications#create
         * @methodOf lbServices.CarModel.carModifications
         *
         * @description
         *
         * Creates a new instance in carModifications of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `CarModification` object.)
         * </em>
         */
        R.carModifications.create = function() {
          var TargetResource = $injector.get("CarModification");
          var action = TargetResource["::create::CarModel::carModifications"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.CarModel.carModifications#destroyAll
         * @methodOf lbServices.CarModel.carModifications
         *
         * @description
         *
         * Deletes all carModifications of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.carModifications.destroyAll = function() {
          var TargetResource = $injector.get("CarModification");
          var action = TargetResource["::delete::CarModel::carModifications"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.CarModel.carModifications#destroyById
         * @methodOf lbServices.CarModel.carModifications
         *
         * @description
         *
         * Delete a related item by id for carModifications.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for carModifications
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.carModifications.destroyById = function() {
          var TargetResource = $injector.get("CarModification");
          var action = TargetResource["::destroyById::CarModel::carModifications"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.CarModel.carModifications#findById
         * @methodOf lbServices.CarModel.carModifications
         *
         * @description
         *
         * Find a related item by id for carModifications.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for carModifications
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `CarModification` object.)
         * </em>
         */
        R.carModifications.findById = function() {
          var TargetResource = $injector.get("CarModification");
          var action = TargetResource["::findById::CarModel::carModifications"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.CarModel.carModifications#updateById
         * @methodOf lbServices.CarModel.carModifications
         *
         * @description
         *
         * Update a related item by id for carModifications.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for carModifications
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `CarModification` object.)
         * </em>
         */
        R.carModifications.updateById = function() {
          var TargetResource = $injector.get("CarModification");
          var action = TargetResource["::updateById::CarModel::carModifications"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.CarGeneration
 * @header lbServices.CarGeneration
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `CarGeneration` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "CarGeneration",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/CarGenerations/:id",
      { 'id': '@id' },
      {

        // INTERNAL. Use CarGeneration.carModel() instead.
        "prototype$__get__carModel": {
          url: urlBase + "/CarGenerations/:id/carModel",
          method: "GET"
        },

        // INTERNAL. Use CarGeneration.carModifications.findById() instead.
        "prototype$__findById__carModifications": {
          url: urlBase + "/CarGenerations/:id/carModifications/:fk",
          method: "GET"
        },

        // INTERNAL. Use CarGeneration.carModifications.destroyById() instead.
        "prototype$__destroyById__carModifications": {
          url: urlBase + "/CarGenerations/:id/carModifications/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use CarGeneration.carModifications.updateById() instead.
        "prototype$__updateById__carModifications": {
          url: urlBase + "/CarGenerations/:id/carModifications/:fk",
          method: "PUT"
        },

        // INTERNAL. Use CarGeneration.carModifications() instead.
        "prototype$__get__carModifications": {
          isArray: true,
          url: urlBase + "/CarGenerations/:id/carModifications",
          method: "GET"
        },

        // INTERNAL. Use CarGeneration.carModifications.create() instead.
        "prototype$__create__carModifications": {
          url: urlBase + "/CarGenerations/:id/carModifications",
          method: "POST"
        },

        // INTERNAL. Use CarGeneration.carModifications.destroyAll() instead.
        "prototype$__delete__carModifications": {
          url: urlBase + "/CarGenerations/:id/carModifications",
          method: "DELETE"
        },

        // INTERNAL. Use CarGeneration.carModifications.count() instead.
        "prototype$__count__carModifications": {
          url: urlBase + "/CarGenerations/:id/carModifications/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.CarGeneration#create
         * @methodOf lbServices.CarGeneration
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `CarGeneration` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/CarGenerations",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.CarGeneration#upsert
         * @methodOf lbServices.CarGeneration
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `CarGeneration` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/CarGenerations",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name lbServices.CarGeneration#exists
         * @methodOf lbServices.CarGeneration
         *
         * @description
         *
         * Check whether a model instance exists in the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/CarGenerations/:id/exists",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.CarGeneration#findById
         * @methodOf lbServices.CarGeneration
         *
         * @description
         *
         * Find a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         *  - `filter` – `{object=}` - Filter defining fields and include
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `CarGeneration` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/CarGenerations/:id",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.CarGeneration#find
         * @methodOf lbServices.CarGeneration
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `CarGeneration` object.)
         * </em>
         */
        "find": {
          isArray: true,
          url: urlBase + "/CarGenerations",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.CarGeneration#findOne
         * @methodOf lbServices.CarGeneration
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `CarGeneration` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/CarGenerations/findOne",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.CarGeneration#updateAll
         * @methodOf lbServices.CarGeneration
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/CarGenerations/update",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.CarGeneration#deleteById
         * @methodOf lbServices.CarGeneration
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/CarGenerations/:id",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name lbServices.CarGeneration#count
         * @methodOf lbServices.CarGeneration
         *
         * @description
         *
         * Count instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/CarGenerations/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.CarGeneration#prototype$updateAttributes
         * @methodOf lbServices.CarGeneration
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `CarGeneration` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/CarGenerations/:id",
          method: "PUT"
        },

        // INTERNAL. Use CarModel.carGenerations.findById() instead.
        "::findById::CarModel::carGenerations": {
          url: urlBase + "/CarModels/:id/carGenerations/:fk",
          method: "GET"
        },

        // INTERNAL. Use CarModel.carGenerations.destroyById() instead.
        "::destroyById::CarModel::carGenerations": {
          url: urlBase + "/CarModels/:id/carGenerations/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use CarModel.carGenerations.updateById() instead.
        "::updateById::CarModel::carGenerations": {
          url: urlBase + "/CarModels/:id/carGenerations/:fk",
          method: "PUT"
        },

        // INTERNAL. Use CarModel.carGenerations() instead.
        "::get::CarModel::carGenerations": {
          isArray: true,
          url: urlBase + "/CarModels/:id/carGenerations",
          method: "GET"
        },

        // INTERNAL. Use CarModel.carGenerations.create() instead.
        "::create::CarModel::carGenerations": {
          url: urlBase + "/CarModels/:id/carGenerations",
          method: "POST"
        },

        // INTERNAL. Use CarModel.carGenerations.destroyAll() instead.
        "::delete::CarModel::carGenerations": {
          url: urlBase + "/CarModels/:id/carGenerations",
          method: "DELETE"
        },

        // INTERNAL. Use CarModel.carGenerations.count() instead.
        "::count::CarModel::carGenerations": {
          url: urlBase + "/CarModels/:id/carGenerations/count",
          method: "GET"
        },

        // INTERNAL. Use CarModification.carGeneration() instead.
        "::get::CarModification::carGeneration": {
          url: urlBase + "/CarModifications/:id/carGeneration",
          method: "GET"
        },
      }
    );



        /**
         * @ngdoc method
         * @name lbServices.CarGeneration#updateOrCreate
         * @methodOf lbServices.CarGeneration
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `CarGeneration` object.)
         * </em>
         */
        R["updateOrCreate"] = R["upsert"];

        /**
         * @ngdoc method
         * @name lbServices.CarGeneration#update
         * @methodOf lbServices.CarGeneration
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["update"] = R["updateAll"];

        /**
         * @ngdoc method
         * @name lbServices.CarGeneration#destroyById
         * @methodOf lbServices.CarGeneration
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["destroyById"] = R["deleteById"];

        /**
         * @ngdoc method
         * @name lbServices.CarGeneration#removeById
         * @methodOf lbServices.CarGeneration
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["removeById"] = R["deleteById"];


    /**
    * @ngdoc property
    * @name lbServices.CarGeneration#modelName
    * @propertyOf lbServices.CarGeneration
    * @description
    * The name of the model represented by this $resource,
    * i.e. `CarGeneration`.
    */
    R.modelName = "CarGeneration";


        /**
         * @ngdoc method
         * @name lbServices.CarGeneration#carModel
         * @methodOf lbServices.CarGeneration
         *
         * @description
         *
         * Fetches belongsTo relation carModel.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `CarModel` object.)
         * </em>
         */
        R.carModel = function() {
          var TargetResource = $injector.get("CarModel");
          var action = TargetResource["::get::CarGeneration::carModel"];
          return action.apply(R, arguments);
        };
    /**
     * @ngdoc object
     * @name lbServices.CarGeneration.carModifications
     * @header lbServices.CarGeneration.carModifications
     * @object
     * @description
     *
     * The object `CarGeneration.carModifications` groups methods
     * manipulating `CarModification` instances related to `CarGeneration`.
     *
     * Call {@link lbServices.CarGeneration#carModifications CarGeneration.carModifications()}
     * to query all related instances.
     */


        /**
         * @ngdoc method
         * @name lbServices.CarGeneration#carModifications
         * @methodOf lbServices.CarGeneration
         *
         * @description
         *
         * Queries carModifications of CarGeneration.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `CarModification` object.)
         * </em>
         */
        R.carModifications = function() {
          var TargetResource = $injector.get("CarModification");
          var action = TargetResource["::get::CarGeneration::carModifications"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.CarGeneration.carModifications#count
         * @methodOf lbServices.CarGeneration.carModifications
         *
         * @description
         *
         * Counts carModifications of CarGeneration.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        R.carModifications.count = function() {
          var TargetResource = $injector.get("CarModification");
          var action = TargetResource["::count::CarGeneration::carModifications"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.CarGeneration.carModifications#create
         * @methodOf lbServices.CarGeneration.carModifications
         *
         * @description
         *
         * Creates a new instance in carModifications of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `CarModification` object.)
         * </em>
         */
        R.carModifications.create = function() {
          var TargetResource = $injector.get("CarModification");
          var action = TargetResource["::create::CarGeneration::carModifications"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.CarGeneration.carModifications#destroyAll
         * @methodOf lbServices.CarGeneration.carModifications
         *
         * @description
         *
         * Deletes all carModifications of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.carModifications.destroyAll = function() {
          var TargetResource = $injector.get("CarModification");
          var action = TargetResource["::delete::CarGeneration::carModifications"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.CarGeneration.carModifications#destroyById
         * @methodOf lbServices.CarGeneration.carModifications
         *
         * @description
         *
         * Delete a related item by id for carModifications.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for carModifications
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.carModifications.destroyById = function() {
          var TargetResource = $injector.get("CarModification");
          var action = TargetResource["::destroyById::CarGeneration::carModifications"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.CarGeneration.carModifications#findById
         * @methodOf lbServices.CarGeneration.carModifications
         *
         * @description
         *
         * Find a related item by id for carModifications.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for carModifications
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `CarModification` object.)
         * </em>
         */
        R.carModifications.findById = function() {
          var TargetResource = $injector.get("CarModification");
          var action = TargetResource["::findById::CarGeneration::carModifications"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.CarGeneration.carModifications#updateById
         * @methodOf lbServices.CarGeneration.carModifications
         *
         * @description
         *
         * Update a related item by id for carModifications.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for carModifications
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `CarModification` object.)
         * </em>
         */
        R.carModifications.updateById = function() {
          var TargetResource = $injector.get("CarModification");
          var action = TargetResource["::updateById::CarGeneration::carModifications"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.CarModification
 * @header lbServices.CarModification
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `CarModification` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "CarModification",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/CarModifications/:id",
      { 'id': '@id' },
      {

        // INTERNAL. Use CarModification.carGeneration() instead.
        "prototype$__get__carGeneration": {
          url: urlBase + "/CarModifications/:id/carGeneration",
          method: "GET"
        },

        // INTERNAL. Use CarModification.carModel() instead.
        "prototype$__get__carModel": {
          url: urlBase + "/CarModifications/:id/carModel",
          method: "GET"
        },

        // INTERNAL. Use CarModification.carBodyType() instead.
        "prototype$__get__carBodyType": {
          url: urlBase + "/CarModifications/:id/carBodyType",
          method: "GET"
        },

        // INTERNAL. Use CarModification.cars.findById() instead.
        "prototype$__findById__cars": {
          url: urlBase + "/CarModifications/:id/cars/:fk",
          method: "GET"
        },

        // INTERNAL. Use CarModification.cars.destroyById() instead.
        "prototype$__destroyById__cars": {
          url: urlBase + "/CarModifications/:id/cars/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use CarModification.cars.updateById() instead.
        "prototype$__updateById__cars": {
          url: urlBase + "/CarModifications/:id/cars/:fk",
          method: "PUT"
        },

        // INTERNAL. Use CarModification.cars() instead.
        "prototype$__get__cars": {
          isArray: true,
          url: urlBase + "/CarModifications/:id/cars",
          method: "GET"
        },

        // INTERNAL. Use CarModification.cars.create() instead.
        "prototype$__create__cars": {
          url: urlBase + "/CarModifications/:id/cars",
          method: "POST"
        },

        // INTERNAL. Use CarModification.cars.destroyAll() instead.
        "prototype$__delete__cars": {
          url: urlBase + "/CarModifications/:id/cars",
          method: "DELETE"
        },

        // INTERNAL. Use CarModification.cars.count() instead.
        "prototype$__count__cars": {
          url: urlBase + "/CarModifications/:id/cars/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.CarModification#create
         * @methodOf lbServices.CarModification
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `CarModification` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/CarModifications",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.CarModification#upsert
         * @methodOf lbServices.CarModification
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `CarModification` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/CarModifications",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name lbServices.CarModification#exists
         * @methodOf lbServices.CarModification
         *
         * @description
         *
         * Check whether a model instance exists in the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/CarModifications/:id/exists",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.CarModification#findById
         * @methodOf lbServices.CarModification
         *
         * @description
         *
         * Find a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         *  - `filter` – `{object=}` - Filter defining fields and include
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `CarModification` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/CarModifications/:id",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.CarModification#find
         * @methodOf lbServices.CarModification
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `CarModification` object.)
         * </em>
         */
        "find": {
          isArray: true,
          url: urlBase + "/CarModifications",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.CarModification#findOne
         * @methodOf lbServices.CarModification
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `CarModification` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/CarModifications/findOne",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.CarModification#updateAll
         * @methodOf lbServices.CarModification
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/CarModifications/update",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.CarModification#deleteById
         * @methodOf lbServices.CarModification
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/CarModifications/:id",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name lbServices.CarModification#count
         * @methodOf lbServices.CarModification
         *
         * @description
         *
         * Count instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/CarModifications/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.CarModification#prototype$updateAttributes
         * @methodOf lbServices.CarModification
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `CarModification` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/CarModifications/:id",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name lbServices.CarModification#importAutoRuModifications
         * @methodOf lbServices.CarModification
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         *  - `filename` – `{string}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `status` – `{string=}` - 
         */
        "importAutoRuModifications": {
          url: urlBase + "/CarModifications/importAutoRuModifications",
          method: "POST"
        },

        // INTERNAL. Use CarModel.carModifications.findById() instead.
        "::findById::CarModel::carModifications": {
          url: urlBase + "/CarModels/:id/carModifications/:fk",
          method: "GET"
        },

        // INTERNAL. Use CarModel.carModifications.destroyById() instead.
        "::destroyById::CarModel::carModifications": {
          url: urlBase + "/CarModels/:id/carModifications/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use CarModel.carModifications.updateById() instead.
        "::updateById::CarModel::carModifications": {
          url: urlBase + "/CarModels/:id/carModifications/:fk",
          method: "PUT"
        },

        // INTERNAL. Use CarModel.carModifications() instead.
        "::get::CarModel::carModifications": {
          isArray: true,
          url: urlBase + "/CarModels/:id/carModifications",
          method: "GET"
        },

        // INTERNAL. Use CarModel.carModifications.create() instead.
        "::create::CarModel::carModifications": {
          url: urlBase + "/CarModels/:id/carModifications",
          method: "POST"
        },

        // INTERNAL. Use CarModel.carModifications.destroyAll() instead.
        "::delete::CarModel::carModifications": {
          url: urlBase + "/CarModels/:id/carModifications",
          method: "DELETE"
        },

        // INTERNAL. Use CarModel.carModifications.count() instead.
        "::count::CarModel::carModifications": {
          url: urlBase + "/CarModels/:id/carModifications/count",
          method: "GET"
        },

        // INTERNAL. Use CarGeneration.carModifications.findById() instead.
        "::findById::CarGeneration::carModifications": {
          url: urlBase + "/CarGenerations/:id/carModifications/:fk",
          method: "GET"
        },

        // INTERNAL. Use CarGeneration.carModifications.destroyById() instead.
        "::destroyById::CarGeneration::carModifications": {
          url: urlBase + "/CarGenerations/:id/carModifications/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use CarGeneration.carModifications.updateById() instead.
        "::updateById::CarGeneration::carModifications": {
          url: urlBase + "/CarGenerations/:id/carModifications/:fk",
          method: "PUT"
        },

        // INTERNAL. Use CarGeneration.carModifications() instead.
        "::get::CarGeneration::carModifications": {
          isArray: true,
          url: urlBase + "/CarGenerations/:id/carModifications",
          method: "GET"
        },

        // INTERNAL. Use CarGeneration.carModifications.create() instead.
        "::create::CarGeneration::carModifications": {
          url: urlBase + "/CarGenerations/:id/carModifications",
          method: "POST"
        },

        // INTERNAL. Use CarGeneration.carModifications.destroyAll() instead.
        "::delete::CarGeneration::carModifications": {
          url: urlBase + "/CarGenerations/:id/carModifications",
          method: "DELETE"
        },

        // INTERNAL. Use CarGeneration.carModifications.count() instead.
        "::count::CarGeneration::carModifications": {
          url: urlBase + "/CarGenerations/:id/carModifications/count",
          method: "GET"
        },

        // INTERNAL. Use Car.carModification() instead.
        "::get::Car::carModification": {
          url: urlBase + "/Cars/:id/carModification",
          method: "GET"
        },
      }
    );



        /**
         * @ngdoc method
         * @name lbServices.CarModification#updateOrCreate
         * @methodOf lbServices.CarModification
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `CarModification` object.)
         * </em>
         */
        R["updateOrCreate"] = R["upsert"];

        /**
         * @ngdoc method
         * @name lbServices.CarModification#update
         * @methodOf lbServices.CarModification
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["update"] = R["updateAll"];

        /**
         * @ngdoc method
         * @name lbServices.CarModification#destroyById
         * @methodOf lbServices.CarModification
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["destroyById"] = R["deleteById"];

        /**
         * @ngdoc method
         * @name lbServices.CarModification#removeById
         * @methodOf lbServices.CarModification
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["removeById"] = R["deleteById"];


    /**
    * @ngdoc property
    * @name lbServices.CarModification#modelName
    * @propertyOf lbServices.CarModification
    * @description
    * The name of the model represented by this $resource,
    * i.e. `CarModification`.
    */
    R.modelName = "CarModification";


        /**
         * @ngdoc method
         * @name lbServices.CarModification#carGeneration
         * @methodOf lbServices.CarModification
         *
         * @description
         *
         * Fetches belongsTo relation carGeneration.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `CarGeneration` object.)
         * </em>
         */
        R.carGeneration = function() {
          var TargetResource = $injector.get("CarGeneration");
          var action = TargetResource["::get::CarModification::carGeneration"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.CarModification#carModel
         * @methodOf lbServices.CarModification
         *
         * @description
         *
         * Fetches belongsTo relation carModel.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `CarModel` object.)
         * </em>
         */
        R.carModel = function() {
          var TargetResource = $injector.get("CarModel");
          var action = TargetResource["::get::CarModification::carModel"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.CarModification#carBodyType
         * @methodOf lbServices.CarModification
         *
         * @description
         *
         * Fetches belongsTo relation carBodyType.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `CarBodyType` object.)
         * </em>
         */
        R.carBodyType = function() {
          var TargetResource = $injector.get("CarBodyType");
          var action = TargetResource["::get::CarModification::carBodyType"];
          return action.apply(R, arguments);
        };
    /**
     * @ngdoc object
     * @name lbServices.CarModification.cars
     * @header lbServices.CarModification.cars
     * @object
     * @description
     *
     * The object `CarModification.cars` groups methods
     * manipulating `Car` instances related to `CarModification`.
     *
     * Call {@link lbServices.CarModification#cars CarModification.cars()}
     * to query all related instances.
     */


        /**
         * @ngdoc method
         * @name lbServices.CarModification#cars
         * @methodOf lbServices.CarModification
         *
         * @description
         *
         * Queries cars of CarModification.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Car` object.)
         * </em>
         */
        R.cars = function() {
          var TargetResource = $injector.get("Car");
          var action = TargetResource["::get::CarModification::cars"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.CarModification.cars#count
         * @methodOf lbServices.CarModification.cars
         *
         * @description
         *
         * Counts cars of CarModification.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        R.cars.count = function() {
          var TargetResource = $injector.get("Car");
          var action = TargetResource["::count::CarModification::cars"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.CarModification.cars#create
         * @methodOf lbServices.CarModification.cars
         *
         * @description
         *
         * Creates a new instance in cars of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Car` object.)
         * </em>
         */
        R.cars.create = function() {
          var TargetResource = $injector.get("Car");
          var action = TargetResource["::create::CarModification::cars"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.CarModification.cars#destroyAll
         * @methodOf lbServices.CarModification.cars
         *
         * @description
         *
         * Deletes all cars of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.cars.destroyAll = function() {
          var TargetResource = $injector.get("Car");
          var action = TargetResource["::delete::CarModification::cars"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.CarModification.cars#destroyById
         * @methodOf lbServices.CarModification.cars
         *
         * @description
         *
         * Delete a related item by id for cars.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for cars
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.cars.destroyById = function() {
          var TargetResource = $injector.get("Car");
          var action = TargetResource["::destroyById::CarModification::cars"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.CarModification.cars#findById
         * @methodOf lbServices.CarModification.cars
         *
         * @description
         *
         * Find a related item by id for cars.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for cars
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Car` object.)
         * </em>
         */
        R.cars.findById = function() {
          var TargetResource = $injector.get("Car");
          var action = TargetResource["::findById::CarModification::cars"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.CarModification.cars#updateById
         * @methodOf lbServices.CarModification.cars
         *
         * @description
         *
         * Update a related item by id for cars.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for cars
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Car` object.)
         * </em>
         */
        R.cars.updateById = function() {
          var TargetResource = $injector.get("Car");
          var action = TargetResource["::updateById::CarModification::cars"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.CarBodyType
 * @header lbServices.CarBodyType
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `CarBodyType` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "CarBodyType",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/CarBodyTypes/:id",
      { 'id': '@id' },
      {

        /**
         * @ngdoc method
         * @name lbServices.CarBodyType#create
         * @methodOf lbServices.CarBodyType
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `CarBodyType` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/CarBodyTypes",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.CarBodyType#upsert
         * @methodOf lbServices.CarBodyType
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `CarBodyType` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/CarBodyTypes",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name lbServices.CarBodyType#exists
         * @methodOf lbServices.CarBodyType
         *
         * @description
         *
         * Check whether a model instance exists in the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/CarBodyTypes/:id/exists",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.CarBodyType#findById
         * @methodOf lbServices.CarBodyType
         *
         * @description
         *
         * Find a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         *  - `filter` – `{object=}` - Filter defining fields and include
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `CarBodyType` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/CarBodyTypes/:id",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.CarBodyType#find
         * @methodOf lbServices.CarBodyType
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `CarBodyType` object.)
         * </em>
         */
        "find": {
          isArray: true,
          url: urlBase + "/CarBodyTypes",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.CarBodyType#findOne
         * @methodOf lbServices.CarBodyType
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `CarBodyType` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/CarBodyTypes/findOne",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.CarBodyType#updateAll
         * @methodOf lbServices.CarBodyType
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/CarBodyTypes/update",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.CarBodyType#deleteById
         * @methodOf lbServices.CarBodyType
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/CarBodyTypes/:id",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name lbServices.CarBodyType#count
         * @methodOf lbServices.CarBodyType
         *
         * @description
         *
         * Count instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/CarBodyTypes/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.CarBodyType#prototype$updateAttributes
         * @methodOf lbServices.CarBodyType
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `CarBodyType` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/CarBodyTypes/:id",
          method: "PUT"
        },

        // INTERNAL. Use CarModification.carBodyType() instead.
        "::get::CarModification::carBodyType": {
          url: urlBase + "/CarModifications/:id/carBodyType",
          method: "GET"
        },
      }
    );



        /**
         * @ngdoc method
         * @name lbServices.CarBodyType#updateOrCreate
         * @methodOf lbServices.CarBodyType
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `CarBodyType` object.)
         * </em>
         */
        R["updateOrCreate"] = R["upsert"];

        /**
         * @ngdoc method
         * @name lbServices.CarBodyType#update
         * @methodOf lbServices.CarBodyType
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["update"] = R["updateAll"];

        /**
         * @ngdoc method
         * @name lbServices.CarBodyType#destroyById
         * @methodOf lbServices.CarBodyType
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["destroyById"] = R["deleteById"];

        /**
         * @ngdoc method
         * @name lbServices.CarBodyType#removeById
         * @methodOf lbServices.CarBodyType
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["removeById"] = R["deleteById"];


    /**
    * @ngdoc property
    * @name lbServices.CarBodyType#modelName
    * @propertyOf lbServices.CarBodyType
    * @description
    * The name of the model represented by this $resource,
    * i.e. `CarBodyType`.
    */
    R.modelName = "CarBodyType";


    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.Car
 * @header lbServices.Car
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Car` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "Car",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/Cars/:id",
      { 'id': '@id' },
      {

        // INTERNAL. Use Car.carModification() instead.
        "prototype$__get__carModification": {
          url: urlBase + "/Cars/:id/carModification",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Car#create
         * @methodOf lbServices.Car
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Car` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/Cars",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.Car#upsert
         * @methodOf lbServices.Car
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Car` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/Cars",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name lbServices.Car#exists
         * @methodOf lbServices.Car
         *
         * @description
         *
         * Check whether a model instance exists in the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/Cars/:id/exists",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Car#findById
         * @methodOf lbServices.Car
         *
         * @description
         *
         * Find a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         *  - `filter` – `{object=}` - Filter defining fields and include
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Car` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/Cars/:id",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Car#find
         * @methodOf lbServices.Car
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Car` object.)
         * </em>
         */
        "find": {
          isArray: true,
          url: urlBase + "/Cars",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Car#findOne
         * @methodOf lbServices.Car
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Car` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/Cars/findOne",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Car#updateAll
         * @methodOf lbServices.Car
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/Cars/update",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.Car#deleteById
         * @methodOf lbServices.Car
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/Cars/:id",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name lbServices.Car#count
         * @methodOf lbServices.Car
         *
         * @description
         *
         * Count instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/Cars/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Car#prototype$updateAttributes
         * @methodOf lbServices.Car
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Car` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/Cars/:id",
          method: "PUT"
        },

        // INTERNAL. Use CarModification.cars.findById() instead.
        "::findById::CarModification::cars": {
          url: urlBase + "/CarModifications/:id/cars/:fk",
          method: "GET"
        },

        // INTERNAL. Use CarModification.cars.destroyById() instead.
        "::destroyById::CarModification::cars": {
          url: urlBase + "/CarModifications/:id/cars/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use CarModification.cars.updateById() instead.
        "::updateById::CarModification::cars": {
          url: urlBase + "/CarModifications/:id/cars/:fk",
          method: "PUT"
        },

        // INTERNAL. Use CarModification.cars() instead.
        "::get::CarModification::cars": {
          isArray: true,
          url: urlBase + "/CarModifications/:id/cars",
          method: "GET"
        },

        // INTERNAL. Use CarModification.cars.create() instead.
        "::create::CarModification::cars": {
          url: urlBase + "/CarModifications/:id/cars",
          method: "POST"
        },

        // INTERNAL. Use CarModification.cars.destroyAll() instead.
        "::delete::CarModification::cars": {
          url: urlBase + "/CarModifications/:id/cars",
          method: "DELETE"
        },

        // INTERNAL. Use CarModification.cars.count() instead.
        "::count::CarModification::cars": {
          url: urlBase + "/CarModifications/:id/cars/count",
          method: "GET"
        },
      }
    );



        /**
         * @ngdoc method
         * @name lbServices.Car#updateOrCreate
         * @methodOf lbServices.Car
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Car` object.)
         * </em>
         */
        R["updateOrCreate"] = R["upsert"];

        /**
         * @ngdoc method
         * @name lbServices.Car#update
         * @methodOf lbServices.Car
         *
         * @description
         *
         * Update instances of the model matched by where from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["update"] = R["updateAll"];

        /**
         * @ngdoc method
         * @name lbServices.Car#destroyById
         * @methodOf lbServices.Car
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["destroyById"] = R["deleteById"];

        /**
         * @ngdoc method
         * @name lbServices.Car#removeById
         * @methodOf lbServices.Car
         *
         * @description
         *
         * Delete a model instance by id from the data source.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["removeById"] = R["deleteById"];


    /**
    * @ngdoc property
    * @name lbServices.Car#modelName
    * @propertyOf lbServices.Car
    * @description
    * The name of the model represented by this $resource,
    * i.e. `Car`.
    */
    R.modelName = "Car";


        /**
         * @ngdoc method
         * @name lbServices.Car#carModification
         * @methodOf lbServices.Car
         *
         * @description
         *
         * Fetches belongsTo relation carModification.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `CarModification` object.)
         * </em>
         */
        R.carModification = function() {
          var TargetResource = $injector.get("CarModification");
          var action = TargetResource["::get::Car::carModification"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.Upload
 * @header lbServices.Upload
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Upload` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "Upload",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/Uploads/:id",
      { 'id': '@id' },
      {

        /**
         * @ngdoc method
         * @name lbServices.Upload#getContainers
         * @methodOf lbServices.Upload
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Upload` object.)
         * </em>
         */
        "getContainers": {
          isArray: true,
          url: urlBase + "/Uploads",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Upload#createContainer
         * @methodOf lbServices.Upload
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Upload` object.)
         * </em>
         */
        "createContainer": {
          url: urlBase + "/Uploads",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.Upload#destroyContainer
         * @methodOf lbServices.Upload
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `container` – `{string=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `` – `{undefined=}` - 
         */
        "destroyContainer": {
          url: urlBase + "/Uploads/:container",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name lbServices.Upload#getContainer
         * @methodOf lbServices.Upload
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `container` – `{string=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Upload` object.)
         * </em>
         */
        "getContainer": {
          url: urlBase + "/Uploads/:container",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Upload#getFiles
         * @methodOf lbServices.Upload
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `container` – `{string=}` - 
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Upload` object.)
         * </em>
         */
        "getFiles": {
          isArray: true,
          url: urlBase + "/Uploads/:container/files",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Upload#getFile
         * @methodOf lbServices.Upload
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `container` – `{string=}` - 
         *
         *  - `file` – `{string=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Upload` object.)
         * </em>
         */
        "getFile": {
          url: urlBase + "/Uploads/:container/files/:file",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Upload#removeFile
         * @methodOf lbServices.Upload
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `container` – `{string=}` - 
         *
         *  - `file` – `{string=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `` – `{undefined=}` - 
         */
        "removeFile": {
          url: urlBase + "/Uploads/:container/files/:file",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name lbServices.Upload#upload
         * @methodOf lbServices.Upload
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         *  - `req` – `{object=}` - 
         *
         *  - `res` – `{object=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `result` – `{object=}` - 
         */
        "upload": {
          url: urlBase + "/Uploads/:container/upload",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.Upload#download
         * @methodOf lbServices.Upload
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `container` – `{string=}` - 
         *
         *  - `file` – `{string=}` - 
         *
         *  - `res` – `{object=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "download": {
          url: urlBase + "/Uploads/:container/download/:file",
          method: "GET"
        },
      }
    );




    /**
    * @ngdoc property
    * @name lbServices.Upload#modelName
    * @propertyOf lbServices.Upload
    * @description
    * The name of the model represented by this $resource,
    * i.e. `Upload`.
    */
    R.modelName = "Upload";


    return R;
  }]);


module
  .factory('LoopBackAuth', function() {
    var props = ['accessTokenId', 'currentUserId'];
    var propsPrefix = '$LoopBack$';

    function LoopBackAuth() {
      var self = this;
      props.forEach(function(name) {
        self[name] = load(name);
      });
      this.rememberMe = undefined;
      this.currentUserData = null;
    }

    LoopBackAuth.prototype.save = function() {
      var self = this;
      var storage = this.rememberMe ? localStorage : sessionStorage;
      props.forEach(function(name) {
        save(storage, name, self[name]);
      });
    };

    LoopBackAuth.prototype.setUser = function(accessTokenId, userId, userData) {
      this.accessTokenId = accessTokenId;
      this.currentUserId = userId;
      this.currentUserData = userData;
    }

    LoopBackAuth.prototype.clearUser = function() {
      this.accessTokenId = null;
      this.currentUserId = null;
      this.currentUserData = null;
    }

    LoopBackAuth.prototype.clearStorage = function() {
      props.forEach(function(name) {
        save(sessionStorage, name, null);
        save(localStorage, name, null);
      });
    };

    return new LoopBackAuth();

    // Note: LocalStorage converts the value to string
    // We are using empty string as a marker for null/undefined values.
    function save(storage, name, value) {
      var key = propsPrefix + name;
      if (value == null) value = '';
      storage[key] = value;
    }

    function load(name) {
      var key = propsPrefix + name;
      return localStorage[key] || sessionStorage[key] || null;
    }
  })
  .config(['$httpProvider', function($httpProvider) {
    $httpProvider.interceptors.push('LoopBackAuthRequestInterceptor');
  }])
  .factory('LoopBackAuthRequestInterceptor', [ '$q', 'LoopBackAuth',
    function($q, LoopBackAuth) {
      return {
        'request': function(config) {

          // filter out non urlBase requests
          if (config.url.substr(0, urlBase.length) !== urlBase) {
            return config;
          }

          if (LoopBackAuth.accessTokenId) {
            config.headers[authHeader] = LoopBackAuth.accessTokenId;
          } else if (config.__isGetCurrentUser__) {
            // Return a stub 401 error for User.getCurrent() when
            // there is no user logged in
            var res = {
              body: { error: { status: 401 } },
              status: 401,
              config: config,
              headers: function() { return undefined; }
            };
            return $q.reject(res);
          }
          return config || $q.when(config);
        }
      }
    }])

  /**
   * @ngdoc object
   * @name lbServices.LoopBackResourceProvider
   * @header lbServices.LoopBackResourceProvider
   * @description
   * Use `LoopBackResourceProvider` to change the global configuration
   * settings used by all models. Note that the provider is available
   * to Configuration Blocks only, see
   * {@link https://docs.angularjs.org/guide/module#module-loading-dependencies Module Loading & Dependencies}
   * for more details.
   *
   * ## Example
   *
   * ```js
   * angular.module('app')
   *  .config(function(LoopBackResourceProvider) {
   *     LoopBackResourceProvider.setAuthHeader('X-Access-Token');
   *  });
   * ```
   */
  .provider('LoopBackResource', function LoopBackResourceProvider() {
    /**
     * @ngdoc method
     * @name lbServices.LoopBackResourceProvider#setAuthHeader
     * @methodOf lbServices.LoopBackResourceProvider
     * @param {string} header The header name to use, e.g. `X-Access-Token`
     * @description
     * Configure the REST transport to use a different header for sending
     * the authentication token. It is sent in the `Authorization` header
     * by default.
     */
    this.setAuthHeader = function(header) {
      authHeader = header;
    };

    /**
     * @ngdoc method
     * @name lbServices.LoopBackResourceProvider#setUrlBase
     * @methodOf lbServices.LoopBackResourceProvider
     * @param {string} url The URL to use, e.g. `/api` or `//example.com/api`.
     * @description
     * Change the URL of the REST API server. By default, the URL provided
     * to the code generator (`lb-ng` or `grunt-loopback-sdk-angular`) is used.
     */
    this.setUrlBase = function(url) {
      urlBase = url;
    };

    this.$get = ['$resource', function($resource) {
      return function(url, params, actions) {
        var resource = $resource(url, params, actions);

        // Angular always calls POST on $save()
        // This hack is based on
        // http://kirkbushell.me/angular-js-using-ng-resource-in-a-more-restful-manner/
        resource.prototype.$save = function(success, error) {
          // Fortunately, LoopBack provides a convenient `upsert` method
          // that exactly fits our needs.
          var result = resource.upsert.call(this, {}, this, success, error);
          return result.$promise || result;
        };
        return resource;
      };
    }];
  });

})(window, window.angular);
