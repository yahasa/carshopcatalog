/**
 * @file Модуль каталога автомобилей
 */
(function() {
  "use strict";
  angular.module('app.catalog', [
    'lbServices',
    'ui.router',
    'ui.bootstrap',
    'ui.bootstrap.showErrors'
  ]).config(catalogRouteConfig);

  function catalogRouteConfig($stateProvider) {
    $stateProvider
      .state('catalog', {
        url: '/catalog',
        templateUrl: 'app/catalog/catalog.html',
        controller: 'CatalogController',
        controllerAs: 'vm',
        authenticate: true
      });
  }

})();
