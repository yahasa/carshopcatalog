"use strict";
/**
 * Контроллер диалогового окна редактирования, создания автомобиля
 */
(function() {
  angular.module('app.catalog')
    .controller('CarDialogController', CarDialogController);

  function CarDialogController(carId, Car, CarMark, CarModel, CarModification, $rootScope, $modalInstance) {
    var vm = this;

    // TODO Получить MINYEAR из настроек
    var MAXYEAR = new Date().getFullYear();
    var MINYEAR = 1990;
    vm.years = [];
    vm.carMarks = [];
    vm.carModels = [];
    vm.carModifications = [];
    vm.carMarkId = null;
    vm.carModelId = null;

    getMarks();
    getYears();

    if (carId) {
      // Редактирование автомобиля
      Car.findOne({
        filter: {
          where: {
            id: carId
          },
          include: {carModification: {carModel: 'carMark'}}
        }
      }, function(car) {
        vm.tCar = car;
        // запомним carMarkId, carModelId в корне tCar
        vm.carMarkId = vm.tCar.carModification.carModel.carMarkId;
        vm.carModelId = vm.tCar.carModification.carModelId;
        getModels();
        getModifications();
      }, function(err) {
        // TODO обработать отсутствие автомобиля?
        vm.tCar = new Car;
        console.log(err);
      });
    } else {
      // Создание автомобиля
      vm.tCar = new Car;
    }

    vm.onCarMarkChange = function() {
      vm.carModelId = null;
      getModels();
      vm.tCar.carModificationId = null;
      getModifications();
    };

    vm.onYearChange = function() {
      vm.carModelId = null;
      getModels();
      vm.tCar.carModificationId = null;
      getModifications();
    };

    vm.onCarModelChange = function() {
      vm.tCar.carModificationId = null;
      getModifications();
    };

    vm.save = function () {
      if (vm.carForm.$dirty) {
        if (vm.carForm.$valid) {
          // Могут быть ошибки, если присутствуют недопустимые поля, поэтому заначим и удалим их
          var tmpModification = vm.tCar.carModification;
          delete vm.tCar.carModification;
          vm.tCar.$save(function(result) {
            $modalInstance.close();
            $rootScope.$state.go($rootScope.$state.current, {}, {reload: true});
          }, function(err) {
            // Не удалось сохранить автомобиль
            // вернем удаленное поле
            vm.tCar.carModification = tmpModification;
            console.log(err);
          });
        } else {
          // Не валидная форма
          console.log('not valid');
        }
      } else {
        // Нечего изменять, форма чистая
        console.log('pristine');
        $modalInstance.close();
      }
    };

    vm.cancel = function () {
      $modalInstance.dismiss('cancel');
    };

    function getMarks() {
      CarMark.find({
        filter: {
          order: 'name ASC'
        }
      }, function(marks) {
        vm.carMarks = marks;
      });
    }

    function getYears() {
      vm.years = [];
      for (var i = MAXYEAR; i>= MINYEAR; i--) {
        vm.years.push(i);
      }
    }

    function getModels() {
      vm.carModels = [];
      // TODO ??? Позволять ли пустой год
      if (vm.carMarkId && vm.tCar.year) {
        CarMark.carModelsByYear({id: vm.carMarkId, year: vm.tCar.year}, function(models) {
          vm.carModels = models;
        }, function(err) {
          console.log(err);
        });
      }
    }

    function getModifications() {
      vm.carModifications = [];
      if (vm.carModelId && vm.tCar.year) {
        CarModel.carModificationsByYear({id: vm.carModelId, year: vm.tCar.year}, function(modifications) {
          vm.carModifications = modifications;
        }, function(err) {
          console.log(err);
        });
      }
    }
  }

})();
