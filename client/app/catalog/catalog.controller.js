/**
 * @file Контроллер каталога автомобилей
 */
(function(angular, undefined) {
  "use strict";

  angular.module('app.catalog')
    .controller('CatalogController', CatalogController);

  function CatalogController(Car, $rootScope, $modal) {
    var vm = this;

    loadCars();

    vm.carDialog = function(carId) {
      var modalInstance = $modal.open({
        animation: true,
        templateUrl: 'app/catalog/car.dialog.html',
        controller: 'CarDialogController',
        controllerAs: 'vm',
        backdrop: 'static',
        size: 'lg',
        resolve: {
          carId: function() {return carId;}
        }
      });
    };

    function loadCars() {
      vm.cars = [];
      Car.find({
        filter: {
          include: {carModification: {carModel: 'carMark'}},
          order: 'id ASC'
        }
      }, function(result) {
        vm.cars = result;
        if (vm.cars.length) {
          vm.hot.loadData(vm.cars);
          vm.hot.render();
        }
      });
    }

    /**
     * Диалоговое окно просмотра и редактирования автомобиля на складе
     * Обработчик вызывается таблицей htable с параметром id автомобиля
     * @param carId id автомобиля
     */
    vm.htableOnEditButtonClick = function(carId) {
      vm.carDialog(carId);
    }
  }

})(angular);
