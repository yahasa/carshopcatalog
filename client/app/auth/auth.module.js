/**
 * @file Модуль авторизации и аутентификации
 */
(function() {
  "use strict";
  angular.module('app.auth', [
    'lbServices',
    'ui.router',
    'ui.bootstrap'
  ]).config(authUnauthorizedHandling)
    .config(authRouteConfig);

  function authUnauthorizedHandling($httpProvider) {
    // http://docs.strongloop.com/display/public/LB/AngularJS+JavaScript+SDK#AngularJSJavaScriptSDK-Handling401Unauthorized
    $httpProvider.interceptors.push(function(LoopBackAuth, $q, $location) {
      return {
        responseError: function(rejection) {
          if (rejection.status == 401) {
            //Now clearing the loopback values from client browser for safe logout...
            LoopBackAuth.clearUser();
            LoopBackAuth.clearStorage();
            $location.nextAfterLogin = $location.path();
            $location.path('/login');
          }
          return $q.reject(rejection);
        }
      };
    });
  }

  function authRouteConfig($stateProvider) {
    $stateProvider
      .state('login', {
        url: '/login',
        templateUrl: 'app/auth/login.html',
        controller: 'LoginController',
        controllerAs: 'vm',
        authenticate: false
      })
      .state('logout', {
        url: '/logout',
        onEnter: function (User, $state) {
          User.logout(function () {
            $state.go('login');
          });
        },
        authenticate: false
      })
      .state('profile', {
        url: '/profile',
        templateUrl: 'app/auth/profile.html',
        authenticate: true
      });
  }

})();
