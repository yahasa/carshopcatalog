/**
 * @file Контроллер страницы входа
 */
(function(angular, undefined) {
  "use strict";
  angular.module('app.auth')
    .controller('LoginController', LoginController);

  function LoginController(User, $state, $location) {
    var vm = this;

    vm.loginData = {
      email: '',
      password: '',
      rememberMe: false
    };

    // Восстанавливаем предыдушую страницу, если нас выкинуло на login
    // http://docs.strongloop.com/display/public/LB/AngularJS+JavaScript+SDK#AngularJSJavaScriptSDK-Handling401Unauthorized
    vm.login = function() {
      User.login({
        rememberMe: vm.loginData.rememberMe
      }, {
        email: vm.loginData.email,
        password: vm.loginData.password
      }, function() {
        // закеширует данные пользователя
        User.getCurrent();
        vm.loginData = {
          email: '',
          password: '',
          rememberMe: false
        };
        var next = $state.nextAfterLogin || 'catalog';
        $state.go(next);
      });
    };
  }
})(angular);
