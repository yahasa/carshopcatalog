/**
 * @file Главный модуль приложения
 */
(function() {
  "use strict";
  angular.module('app', [
    'lbServices',
    'ui.router',
    'ui.bootstrap',
    'app.components',
    'app.catalog',
    'app.auth',
    'app.settings'
  ]).config(appRouteConfig)
    .run(appRouteRun);

  function appRouteConfig($stateProvider, $urlRouterProvider) {

    // Хорошо бы 'catalog'  по умолчанию, но проблема: https://github.com/angular-ui/ui-router/issues/600
    // Здесь мы не можем проверить пользователя - поэтому кидаем на login, а он сам разберется
    $urlRouterProvider.when('', '/catalog');
    $urlRouterProvider.otherwise('login');
  }

  function appRouteRun(User, $rootScope, $state, $stateParams) {

    // Если сессия сохранена - получаем пользователя
    User.getCurrent();
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;
    $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
      if (User.isAuthenticated()) {
        if (toState.name === 'login') {
          event.preventDefault(); //prevent current page from loading
          $state.go('catalog')
        }
      } else {
        if (toState.authenticate) {
          event.preventDefault(); //prevent current page from loading
          $state.go('login');
        }
      }
    });
  }

})();
