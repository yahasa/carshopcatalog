/**
 * @file Директива для привязки Handsontable
 * Есть ngHandsontable, но с ним могут быть проблемы с рендером
 *
 * В родительском скоупе должен быть обработчик vm.htableOnEditButtonClick(id)
 * Не очень изящно с обработчиком, но по-другому не прокатывает
 */
(function() {
  "use strict";

  angular.module('app.components')
    .directive('htable', hansontableDirective);

  function hansontableDirective($compile) {
    var directive = {};
    directive.restrict = 'A';
    directive.scope = {
      instance: '='
    };
    directive.link = function(scope,element,attrs) {
      var container = angular.element(element);
      var editButton = function(instance, td, row, col, prop, value, cellProperties) {
        var el = angular.element(td);
        var btnHtml = '<button type="button" class="btn btn-default" ng-click="vm.htableOnEditButtonClick('
          + value
          + ')"><span class="glyphicon glyphicon-edit"></span></button>';
        el.empty().append($compile(btnHtml)(scope.$parent)); //empty is needed because you are rendering to an existing cell
        return el;
      };
      var settings = {
        data: [],
        dataSchema: {id: null, carModification: {name: null, carModel: {name: null, carMark: {name: null}}}, addons: null},
        stretchH: 'last',
        wordWrap: false,
        readOnly: true,
        columnSorting: true,
        manualColumnResize: true,
        columns: [
          { title: " ", data: "id", renderer: editButton, disableVisualSelection: true},
          { title: "id", data: "id", className: "htCenter htMiddle"},
          { title: "Марка", data: "carModification.carModel.carMark.name", className: "htLeft htMiddle"},
          { title: "Модель", data: "carModification.carModel.name", width: 80, className: "htLeft htMiddle"},
          { title: "Дата", data: "supplydate", width: 80, className: "htLeft htMiddle"},
          { title: "Мотор", data: "motor", width: 80, className: "htLeft htMiddle"},
          { title: "Комплектация", data: "specs", width: 120, className: "htLeft htMiddle"},
          { title: "Допы", data: "addons", className: "htLeft htMiddle"}
        ]
      };
      scope.instance = new Handsontable( container[0], settings );

      // Применим стили Bootstrap

      var ex = container[0];
      var table = ex.querySelectorAll('table');

      function findAncestor(el, cls) {
        while ((el = el.parentElement) && !el.classList.contains(cls)) return el;
      }

      for (var i = 0, len = table.length; i < len; i++) {
        var tableInstance = table[i];
        var isMaster = !!findAncestor(tableInstance, 'ht_master');

        Handsontable.Dom.addClass(tableInstance, 'table');
        Handsontable.Dom.addClass(tableInstance, 'table-hover');
        //Handsontable.Dom.addClass(tableInstance, 'table-striped');

        if (isMaster) {
          Handsontable.Dom.addClass(tableInstance, 'table-bordered');
        }
      }

    };//--end of link function
    return directive;
  }

})();
