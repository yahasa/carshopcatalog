/**
 * @file Контроллер приложения
 */
(function() {
  "use strict";
  angular.module('app')
    .controller('AppController', AppController);

  function AppController(LoopBackAuth, User) {
    var vm = this;
    vm.isAuthenticated = function() {
      return User.isAuthenticated();
    };
    vm.getCurrentUserData = function() {
      return User.getCachedCurrent();
    };
  }

})();
