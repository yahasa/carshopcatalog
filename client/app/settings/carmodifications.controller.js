/**
 * @file Контроллер страницы редактирования модификаций автомобилей
 */
(function() {
  "use strict";

  angular.module('app.settings')
    .controller('CarModificationsController', CarModificationsController);

  function CarModificationsController(CarMark, CarModel, CarModification, $rootScope, $modal) {
    var vm = this;

    vm.$stateParams = $rootScope.$stateParams;
    vm.selection = {};
    if(vm.$stateParams.markId) {
      CarMark.findById({id: vm.$stateParams.markId}, function(markInstance) {
        vm.selection.mark = markInstance;
        if (vm.$stateParams.modelId) {
          CarModel.findById({id: vm.$stateParams.modelId}, function(modelInstance) {
            vm.selection.model = modelInstance;
            CarModification.find({
              filter: {
                order: ['generation ASC', 'name ASC'],
                where: {
                  carModelId: vm.selection.model.id
                }
              }
            }, function (modifications) {
              vm.modifications = modifications;
            });
          });
        } else {
          vm.models = CarModel.find({filter: {where: {carMarkId: vm.selection.mark.id}, order: 'name ASC'}});
        }
      });
    } else {
      vm.marks = CarMark.find({filter: {where: {id: vm.$stateParams.markId}, order: 'name ASC'}});
    }

    vm.openModificationDialog = function(modification) {
      var modalInstance = $modal.open({
        animation: true,
        templateUrl: 'app/settings/carmodification.dialog.html',
        controller: 'CarModificationDialogController',
        controllerAs: 'vm',
        size: 'lg',
        resolve: {
          modification: function() {return modification;}
        }
      });
    };

    vm.createModification = function() {
      vm.openModificationDialog({
        // TODO Убрать carMarkId
        carMarkId: parseInt(vm.$stateParams.markId) || null,
        carModelId: parseInt(vm.$stateParams.modelId) || null
      });
    };

    vm.deleteModification = function(modification) {
      CarModification.deleteById({id: modification.id}, function(value) {
        $rootScope.$state.go($rootScope.$state.current, {}, {reload: true});
      }, function(err) {
        // TODO Обработать ошибку удаления
        // TODO Диалог подтверждения удаления
      });
    }
  }

})();
