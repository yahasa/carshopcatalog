/**
 * @file Контроллер страницы импорта модификаций автомобилей
 * @const vm.CONTAINER Имя контейнера для импорта
 */
(function() {
  "use strict";
  var CONTAINER = 'import';

  angular.module('app.settings')
    .controller('CarModificationsImportController', CarModificationsImportController);

  function CarModificationsImportController(Upload, CarModification, FileUploader, $http) {
    var vm = this;

    vm.CONTAINERURL = '/api/uploads/' + CONTAINER;

    // TODO Переписать с помощью lbServices
    //Upload.getContainer({container: 'import'}, function(container) {
    //  console.log(container);
    //}, function(err) {
    //  console.log(err);
    //});


    // create a uploader with options
    vm.uploader = new FileUploader({
      url: vm.CONTAINERURL + '/upload',
      formData: [
        { key: 'value' }
      ],
      filters: []
    });

    vm.uploader.onSuccessItem = function(item, response, status, headers) {
      vm.load();
    };

    vm.load = function () {
      $http.get(vm.CONTAINERURL + '/files').success(function (data) {
        vm.files = data;
      });
    };

    vm.delete = function (index, id) {
      $http.delete(vm.CONTAINERURL + '/files/' + encodeURIComponent(id)).success(function (data, status, headers) {
        vm.files.splice(index, 1);
      });
    };

    vm.importModifications = function ($index, name) {
      vm.importStatus = 'import';
      CarModification.importAutoRuModifications({}, {filename: CONTAINER +'/' + name}, function(responce) {
        vm.importStatus = 'success';
        vm.importMessage = responce.message;
      }, function(err) {
        vm.importStatus = 'error';
        vm.importMessage = err.data.error.message;
        console.log(err);
      })
    };

    vm.load();
  }
})();
