/**
 * @file Модуль настроек приложения
 */
(function() {
  "use strict";
  angular.module('app.settings', [
    'lbServices',
    'ui.router',
    'ui.bootstrap',
    'angularFileUpload'
  ]).config(settingsRouteConfig);

  function settingsRouteConfig($stateProvider) {
    $stateProvider
      .state('settings', {
        url: '/settings',
        templateUrl: 'app/settings/settings.html',
        controller: 'SettingsController',
        controllerAs: 'vm',
        authenticate: true
      })
      .state('settings.carmodifications', {
        url: '/carmodifications?markId&modelId&modificatonId',
        templateUrl: 'app/settings/carmodifications.html',
        controller: 'CarModificationsController',
        controllerAs: 'vm',
        authenticate: true
      })
      .state('settings.carmodificationsimport', {
        url: '/carmodifications/import',
        templateUrl: 'app/settings/carmodifications.import.html',
        controller: 'CarModificationsImportController',
        controllerAs: 'vm',
        authenticate: true
      });
  }

})();
