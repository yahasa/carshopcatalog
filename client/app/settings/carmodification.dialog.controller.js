/**
 * @file Контроллер диалогового окна редактирования модификации автомобиля
 */
(function() {
  "use strict";

  angular.module('app.settings')
    .controller('CarModificationDialogController', CarModificationDialogController);

  function CarModificationDialogController(modification, CarMark, CarModel, CarModification, $rootScope, $modalInstance) {
    var vm = this;

    // TODO переписать с angular.copy() ???
    // TODO Диалоги подтверждения
    // TODO Красиво показать валидацию
    // максимальное и минимальное допустимые значения годов

    var MAXYEAR = new Date().getFullYear();
    // TODO Получить MINYEAR из базы
    var MINYEAR = 1950;
    vm.newModification = {};
    vm.marks = CarMark.find();

    /* Обработчики */
    vm.onCarMarkChange = function() {
      vm.models = CarMark.carModels({id: vm.newModification.carMarkId});
      vm.newModification.carModelId = null;
      vm.newModification.generation = null;
    };
    vm.onCarModelChange = function() {
      vm.newModification.generation = null;
    };

    vm.updateYearstart = function() {
      vm.yearstart = [];
      for (var i = (vm.newModification.yearend || MAXYEAR); i>=MINYEAR; i--) {
        vm.yearstart.push(i);
      }
    };

    vm.updateYearend = function() {
      vm.yearend = [];
      for (var i = MAXYEAR; i>=(vm.newModification.yearstart || MINYEAR); i--) {
        vm.yearend.push(i);
      }
    };

    vm.ok = function () {
      if (vm.modificationForm.$dirty) {
        if (vm.modificationForm.$valid) {
          console.log(vm.newModification);
          if (!vm.newModification.id) delete vm.newModification.id;
          if (!vm.newModification.generation) vm.newModification.generation = null;
          if (!vm.newModification.bodytype) vm.newModification.bodytype = null;
          delete vm.newModification.carMarkId;
          CarModification.updateOrCreate({}, vm.newModification, function(result) {
            $modalInstance.close();
            // TODO Переход на новую марку
            $rootScope.$state.go($rootScope.$state.current, {}, {reload: true});
          }, function(err) {
            console.log(err);
          });
        } else {
          console.log('not valid');
        }
      } else {
        // Нечего изменять, форма чистая
        console.log('clear');
        $modalInstance.close();
      }
    };

    vm.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
    /* - Обработчики */


    if (modification.id) {
      CarModification.findOne({
        filter: {
          include: {carModel: 'carMark'},
          where: {
            id: modification.id
          }
        }
      }, function (modificationInstance) {
        vm.modification = modificationInstance;
        vm.newModification.id = modificationInstance.id;
        copyModification(vm.newModification, modificationInstance);
        vm.updateYearstart();
        vm.updateYearend();
        vm.models = CarMark.carModels({id: vm.newModification.carMarkId});
      });
    } else {
      copyModification(vm.newModification, modification);
      vm.updateYearstart();
      vm.updateYearend();
      vm.models = CarMark.carModels({id: vm.newModification.carMarkId});
    }


    function copyModification(toMod, fromMod) {
      toMod.name = fromMod.name || null;
      toMod.carMarkId = parseInt(fromMod.carMarkId) || parseInt(fromMod.carModel.carMarkId) || null;
      toMod.carModelId = parseInt(fromMod.carModelId) || null;
      toMod.generation = fromMod.generation || null;
      toMod.bodytype = fromMod.bodytype || null;
      toMod.yearstart = parseInt(fromMod.yearstart) || null;
      toMod.yearend = parseInt(fromMod.yearend) || null;
    }
  }

})();
